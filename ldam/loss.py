import tensorflow as tf
from tensorflow.python import ops
from tensorflow.python.ops import math_ops as mo
from tensorflow.keras import backend as K


def LDAM(C, label_frequencies, phase=1):
    """
    Label-Distribution-Aware Margin Loss

    :param C: hyperparameter
    :param label_frequencies: list of class frequencies (ie. 2 classes, c0 = 10 examples, c1= 100 --> [10, 100])
    :param phase: whether the loss is being used during 1st or 2nd training pass
    :return: LDAM loss
    """

    def loss(y_true, y_pred):
        # -log( e^(z_y - delta(y)) / (e^(z_y - delta(y)) + SUM_(j!=y)[e^(z_j)] ) )
        #   where delta(j) = C / n_y^.25  for j in {1,...,k}

        y_pred = ops.convert_to_tensor(y_pred)
        y_true = mo.cast(y_true, y_pred.dtype)
        lf = mo.cast(label_frequencies, y_pred.dtype)

        sum_zj = mo.reduce_sum((1 - y_true) * y_pred, axis=-1)  # "neg"
        zy = mo.reduce_sum(y_true*y_pred, axis=-1)  # "pos"
        y = mo.argmax(y_true, axis=1)

        freqs = tf.gather(lf, y)  # Q = label_frequencies[y]
        one_over_four = tf.constant(.25, dtype='float32')
        Q = mo.cast(freqs, y_pred.dtype)
        dy = C / (mo.pow(Q, one_over_four))  # d_y = C / n_y^(1/4)
        e_zydy = mo.exp(zy-dy)

        # loss = -log( e^(z_y - delta(y)) / (e^(z_y - delta(y)) + SUM_(j!=y)[e^(z_j)] ) )
        L = -mo.log(e_zydy / (e_zydy + sum_zj))
        if phase == 2:
            inverse_freqs = mo.inv(freqs)
            L = L * inverse_freqs
            print('L after reweight', K.print_tensor(L))

        return L

    return loss
