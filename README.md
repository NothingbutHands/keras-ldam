# Keras - Label Distribution Aware Margin Loss 
### *A Loss function for Imbalanced Datasets*

A Keras implementation of the LDAM loss function from [Kaidi Cao, et al. "Learning Imbalanced Datasets with Label-Distribution-Aware Margin Loss." (2019)](https://arxiv.org/abs/1906.07413)

